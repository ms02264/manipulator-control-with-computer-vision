#pragma once

#include "open_manipulator_libs/kinematics.h"
#include "open_manipulator_libs/open_manipulator.h"
#include "open_manipulator_msgs/GetKinematicsPose.h"
#include "open_manipulator_msgs/SetKinematicsPose.h"
#include "open_manipulator_cv_controller/JointPositions.h"
#include "open_manipulator_cv_controller/Joints.h"
#include "open_manipulator_cv_controller/ManipulatorPoses.h"
#include <robotis_manipulator/robotis_manipulator.h>
#include "ros/ros.h"
   
class KinematicsSolver
{
public:
  KinematicsSolver(bool using_platform, std::string usb_port, std::string baud_rate, double control_period);
   
  void keypointsInverseCallback(const open_manipulator_cv_controller::ManipulatorPoses &msg);
  void keypointsForwardCallback(const open_manipulator_cv_controller::Joints &msg);
   
  ros::NodeHandle getNodeHandle() const { return n_; } 

private:
  OpenManipulator open_manipulator_;
  std::vector<JointValue>* goal_joint_value_;
   
  ros::NodeHandle n_;
  ros::Publisher ik_pub_;
  ros::Publisher fk_pub_;
  robotis_manipulator::Kinematics *kinematics_;

  void solveIK(Pose target_pose, const open_manipulator_msgs::KinematicsPose& manipulator_pose);
  void solveFK(const open_manipulator_cv_controller::Joints &msg);
};