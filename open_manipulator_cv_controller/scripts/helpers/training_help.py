import torch.nn as nn
import torch.nn.functional as F

class Net(nn.Module):
  def __init__(self, input_num, output_num):
    super(Net, self).__init__()
    self.l1 = nn.Linear(input_num, 16)
    self.l2 = nn.Linear(16, 256)
    self.l3 = nn.Linear(256, output_num)
    self.drop1 = nn.Dropout(0.10, inplace = False)
    self.drop2 = nn.Dropout(0.41, inplace = False)

  def forward(self, x):
    x = self.drop1(self.l1(x))
    x = self.drop2(self.l2(x))
    return self.l3(x)
  

class Net2(nn.Module):
  def __init__(self, input_num, output_num):
    super(Net2, self).__init__()
    self.l1 = nn.Linear(input_num, 128)
    self.l2 = nn.Linear(128, 32)
    self.l3 = nn.Linear(32, output_num)
    self.drop1 = nn.Dropout(0.48, inplace = False)
    self.drop2 = nn.Dropout(0.38, inplace = False)

  def forward(self, x):
    x = nn.Sigmoid()(self.drop1(self.l1(x)))
    x = nn.Sigmoid()(self.drop2(self.l2(x)))
    return self.l3(x)
