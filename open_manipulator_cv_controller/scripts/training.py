import os
import pickle
import rospy
import pypareto
import matplotlib
import torch
import tf
import math
import json

import numpy as np
import pandas as pd
import seaborn as sns
import torch.nn as nn
import torch.optim as optim
import matplotlib.pyplot as plt

from tqdm import tqdm
from random import choice, random, uniform
from open_manipulator_msgs.msg import KinematicsPose
from open_manipulator_cv_controller.msg import Joints

from torch.utils.data import DataLoader
from sklearn.model_selection import train_test_split



dataset_name = rospy.get_param('/training/dataset')
DATASET_PATH = os.path.expanduser(f"~/catkin_ws/src/open_manipulator_cv_controller/data/{dataset_name}")

model_name = rospy.get_param('/training/model')
MODEL_SAVE_PATH = os.path.expanduser(f"~/catkin_ws/src/open_manipulator_cv_controller/data/models/{model_name}")

criterion = rospy.get_param('/training/loss')
scheduler = rospy.get_param('/training/scheduler')
learning_rate = rospy.get_param('/training/initial_lr')
EPOCHS = rospy.get_param('/training/epochs')

PLOT = rospy.get_param('/training/plot')
EVALUATE = rospy.get_param('/training/evaluate')

RUN_NAS = rospy.get_param('/training/NAS')
NAS_iter = rospy.get_param('/training/NAS_iter')

LEARNING_RATE = 1e-1
CRITERION = None
SCHEDULER = None
CHECKPOINT_LOG = 0

INPUTS = 7
OUTPUTS = 4

def validate_params():  
    global CRITERION, SCHEDULER, LEARNING_RATE, CHECKPOINT_LOG, EVALUATE

    if type(EPOCHS) != int:
        print(f"Epochs have to be of integer type")
        return False

    if type(EVALUATE) != bool or type(PLOT) != bool:
        print(f"`evaluate` and `plot` flags need to be booleans")
        return False
    try:
        LEARNING_RATE = float(learning_rate)
    except Exception as conversion_exp:
        print(f"Learning rate incorrect. Exception when converting: {conversion_exp}")
        return False

    if criterion == "mse":
        CRITERION = nn.MSELoss()
    elif criterion == "huber":
        CRITERION = nn.HuberLoss()
    else:
        print(f"Given loss [{criterion}] not valid")
        return False
    
    CHECKPOINT_LOG = 5

    if scheduler == "None":
        SCHEDULER = None
    else:
        print(f"Given scheduler [{scheduler}] not valid")
        return False
    
    if RUN_NAS:
        EVALUATE = False

    return True


def list_to_tensor(arr):
    return torch.tensor(arr)

def process_dataset(dataset):
    inputs = []
    labels = []


    for data in dataset:
        actual_values = []
        for joint in data['jointPositions']:
            actual_values.append(joint.angle)

        labels.append(actual_values)

        man_pos = data['manipulatorPositions']
        man_angle = data['angle']

        man_angle = tf.transformations.euler_from_quaternion([man_angle.x,man_angle.y, man_angle.z, man_angle.w])

        x = [man_pos.x, man_pos.y, man_pos.z, man_angle[1]]
        inputs.append(x)

    inputs, labels = remove_duplicates(inputs, labels)

    inputs = list_to_tensor(np.array(inputs).astype(np.float32))
    labels = list_to_tensor(np.array(labels).astype(np.float32))

    X_train, X_test, y_train, y_test = train_test_split(
        inputs, 
        labels, 
        test_size = 0.20, 
        shuffle = True,
        random_state = 42,
    )

    train_data = [(X_train[i], y_train[i]) for i in range(len(y_train))]
    test_data = [(X_test[i], y_test[i]) for i in range(len(y_test))]

    trainloader = DataLoader(train_data, shuffle=True, batch_size=5)
    testloader = DataLoader(test_data, shuffle=True, batch_size=5)

    return trainloader, testloader
    #return (X_train, y_train), (X_test, y_test)

def remove_duplicates(inputs, labels):
    processed_inputs, processed_labels = [], []
    
    for i in range(len(inputs)):
        
        duplicates = []
        for j in range(i+1, len(inputs)):
            duplicates.append(inputs[i] == inputs[j])    
        
        if np.array(duplicates).any() == False: 
            processed_inputs.append(inputs[i])
            processed_labels.append(labels[i])

    print(f"{len(inputs) - len(processed_inputs)} duplicates have been removed")
    return processed_inputs, processed_labels


def train(train_set, model, optimizer):
    model.train()

    running_loss = 0.0
    for loader in train_set:
        X, y = loader[0], loader[1]

        # zero the parameter gradients
        optimizer.zero_grad()

        # forward pass
        outputs = model(X)

        # calculate the errors
        loss = CRITERION(outputs, y)

        loss.backward()
        optimizer.step()

        running_loss += loss.item()

    return running_loss / len(train_set)


def test(test_set, model):
    model.eval()
    with torch.no_grad():
        
        running_loss = 0.0
        for loader in test_set: 
            X, y = loader[0], loader[1]
            
            outputs = model(X)
            
            loss = CRITERION(outputs, y)

            running_loss += loss.item()
    
    return running_loss / len(test_set)


def save_model(model, path = MODEL_SAVE_PATH):
    model_scripted = torch.jit.script(model)
    model_scripted.save(path)
    print(f"Best model has been saved: {path} \n")


def boxplots(abs_err):
    df = pd.DataFrame(abs_err)

    axes = plt.figure().add_subplot(111)
    sns.set_style("whitegrid")

    boxprops = dict(linestyle='-', linewidth=1.5, color='#00145A')
    flierprops = dict(marker='o', markersize=1,
                    linestyle='none')
    whiskerprops = dict(color='#00145A')
    capprops = dict(color='#00145A')
    medianprops = dict(linewidth=1.5, linestyle='-', color='#01FBEE')

    for row in df.transpose().iterrows():
        label = row[0]
        val = row[1]

        axes.scatter([label + 1 for _ in range(val.shape[0])], val, alpha = 0.4)

    axes.boxplot(df, notch=False, boxprops=boxprops, whiskerprops=whiskerprops,capprops=capprops, flierprops=flierprops, medianprops=medianprops,showmeans=False)  

    labels = [item.get_text() for item in axes.get_xticklabels()]

    for i in range(len(labels)):
        labels[i] = f"joint_{i+1}"

    axes.set_xticklabels(labels)

    plt.xlabel("Joints")
    plt.ylabel("Absolute Joint Error")

    plt.show()

def per_joint_error(loader, predictions, loss_fn):
    errors = {}
    abs_err = {}

    labels = np.array([output.numpy() for data in loader for output in data[1]])
    predictions = np.array([prediction.numpy() for prediction in predictions])

    joint_labels, joint_predictions = np.transpose(labels), np.transpose(predictions)

    for joint in range(joint_labels.shape[0]):

        abs_err[joint] = []
        for label, pred in zip(joint_labels[joint], joint_predictions[joint]):
            abs_err[joint].append(pred - label)

        error = loss_fn(torch.Tensor(joint_labels[joint]), torch.Tensor(joint_predictions[joint]))
        
        print(error)
        errors[f"joint{joint+1}"] = error.item()
    
    print("\n-------------------------------")
    print(f"Per Joint Error: \n{errors}")
    print("\n-------------------------------\n")

    if PLOT:
        boxplots(abs_err)

def plot_history(history):
    print("Plotting the training...")
    train_loss = history['train_loss']
    test_loss = history['test_loss']
    pos_loss = history['pos_test_loss']

    axes = plt.figure().add_subplot(111)

    axes.plot(train_loss, "-b", label = "Training Joint Error")
    axes.plot(test_loss, "-r", label = "Test Joint Error")
    axes.plot(pos_loss, "--g", label = "Test Positional Error")
    axes.legend(loc="upper left")

    plt.xlabel("Epochs")
    plt.ylabel(f"{criterion.upper()} loss")    

    locator = matplotlib.ticker.MultipleLocator(2)
    plt.gca().xaxis.set_major_locator(locator)
    formatter = matplotlib.ticker.StrMethodFormatter("{x:.0f}")
    plt.gca().xaxis.set_major_formatter(formatter)

    labels = [item.get_text() for item in axes.get_xticklabels()]
    
    for idx, epoch in enumerate(range(0, EPOCHS, CHECKPOINT_LOG * 2)):
        labels[idx+1] = epoch 

    axes.set_xticklabels(labels)

    plt.show()

def predict(model, loader):

    model.eval()
    with torch.no_grad():
        inputs = [input for data in loader for input in data[0]]
        return [model(input).detach() for input in inputs]


def positional_loss(prediction, labels):
    predicted_position = solve_fk(prediction)
    actual_position = solve_fk(labels)

    cost_fn = CRITERION
    cost = cost_fn(actual_position, predicted_position)

    return cost


def dataset_pos_error(loader, model):
    inputs = [input for data in loader for input in data[0]]
    labels = [output for data in loader for output in data[1]]

    model.eval()
    with torch.no_grad():

        max_ = 0.0

        counter = 0.0
        loss = 0.0
        for input, label in tqdm(zip(inputs, labels), total = len(inputs)):
            prediction = model(input).detach()
            
            predicted_position = solve_fk(prediction)
            actual_position = solve_fk(label)


            dist = np.linalg.norm(actual_position.numpy())
            if max_ <= dist:
                max_ = dist

            loss += CRITERION(actual_position, predicted_position)
            counter += 1

    return loss / counter

MAX_DIST = 0.504
A, B, C = (0.12, 0.277, 0.070)
DX, DY, DZ = (0.275, 0.0, -0.1)
R1, R2, R3 = MAX_DIST, MAX_DIST, MAX_DIST
SAVE_METRICS = os.path.expanduser("~/catkin_ws/src/open_manipulator_cv_controller/notebooks/")


def trajectory(t):
    x = A * math.cos(t + math.pi) + DX
    y = B * math.sin(t + math.pi) + DY
    z = C * t + DZ
    return (x,y,z)

def workspace(u, v):
  x = R1 * np.cos(u) * np.sin(v)
  y = R2 * np.sin(u) * np.sin(v)
  z = R3 * np.cos(v)

  return x,y,z

def sample_eval_points(model, test_set, name):

    print("Sampling points for testing...")

    model.eval()

    ### Testing Trajectory Location ###
    trajectory_loc = [trajectory(t) for t in np.arange(0, 2 * math.pi, 0.1)]
    
    trajectory_pred = []
    for (x,y,z) in tqdm(trajectory_loc):
        # Input tensor to the model which will not have end-effector rotation

        input = torch.Tensor([x, y, z, 0.0])

        # Getting the joint predictions from the model
        pred = model(input).detach().numpy()
        
        # Using the FK to get the predicted locations
        loc = solve_fk(pred).tolist()

        # Saving the locations
        trajectory_pred.append(loc)
    
    ### Testing Predicted Orientation ###
    
    # Using a starting home position of the manipulator
    starting_x, starting_y, starting_z = 0.286, 0.0, 0.204
    
    # Getting the range of the end-effector rotation
    orientation_loc = list(np.arange(-math.pi, math.pi, 0.1))

    orientation_pred = []
    for orient in tqdm(orientation_loc):
        # Getting the quaternion angle
        # angle = tf.transformations.quaternion_from_euler(0.0, orient, 0.0)
        
        # Creating an input from the orientation and home position
        input = torch.Tensor([starting_x, starting_y, starting_z, orient])
        
        # Getting the prediction from the model for the end-effector joint and saving it
        pred = model(input).detach().tolist()[-1]
        orientation_pred.append(pred)
    
    ### Testing Maximum Workspace ###

    # Sampling points on the maximum range of motion
    max_workspace = [workspace(u,v) 
                    for u in np.arange(0, math.pi, 0.5) 
                    for v in np.arange(0, math.pi, 0.25)]

    max_workspace_pred = []
    for (x,y,z) in tqdm(max_workspace):
        # Input tensor to the model which will not have end-effector rotation
        input = torch.Tensor([x,y,z,0.0])

        # Getting the joint predictions from the model
        pred = model(input).detach().numpy()
        
        # Using the FK to get the predicted locations
        loc = solve_fk(pred).tolist()

        # Saving the locations
        max_workspace_pred.append(loc)


    #### Saving the points and predictions for the test set ####
    predictions, ground_truth = [], []
    model.eval()
    with torch.no_grad():
        test_inputs = [input for data in test_set for input in data[0]][:25]
        test_labels = [output for data in test_set for output in data[1]][:25]

        for input, actual in tqdm(zip(test_inputs, test_labels), total = len(test_inputs)):
            pred = model(input).detach()
            ground_truth.append(solve_fk(actual).tolist())
            predictions.append(solve_fk(pred).tolist())

    dist = max([np.linalg.norm(loc) for loc in ground_truth])

    metrics = json.dumps({
        "pos_traj": (
            trajectory_pred, trajectory_loc
        ),
        "or_traj": (
            orientation_pred, orientation_loc
        ), 
        "workspace": (
            max_workspace_pred, max_workspace
        ),
        "test_set": (
            predictions, ground_truth
        ),
        "max_dist": dist.item()
    })

    path = os.path.join(SAVE_METRICS, name)
    with open(path, "w") as handle:
        print(f"Saving the metrics: {path}")
        json.dump(metrics, handle)

    print("Successfully saved the points")


def evaluate_model(model_dict, train_set, test_set, name = "metrics.json"):
    print("Loading the best model...")     
    model = model_dict['model']
    model.eval()

    with torch.no_grad():
        predictions = predict(model, test_set)

        if not RUN_NAS:

            if PLOT:
                history = model_dict['history'] 
                plot_history(history)
        
            print(f"""Best model was trained with {criterion.upper()}: 
            -> Average Joint Error (train): {model_dict['train_loss']}
            -> Average Joint Error (test): {model_dict['test_loss']}
            -> Average Positional Error (test): {model_dict['pos_err']}\n""")

        else:
            print(f"""Best model was trained with {criterion.upper()}: 
            -> Average Joint Error (test): {model_dict['min_loss']}
            -> Average Positional Error (test): {model_dict['pos_err']}\n""")

        per_joint_error(test_set, predictions, CRITERION)

    sample_eval_points(model, test_set, name = name)
    

def solve_fk(prediction: torch.Tensor):
    global kinematics_pub

    msg = Joints()
    joints = [val.item() for val in prediction]

    msg.angles = joints

    kinematics_pub.publish(msg)

    try:
        return_msg = rospy.wait_for_message('/forward_kinematics_keypoints', KinematicsPose, timeout = rospy.Duration(5.0))
        position = return_msg.pose.position
        position = torch.tensor([position.x, position.y, position.z])
    except Exception:
        rospy.sleep(0.1)
        return_msg = rospy.wait_for_message('/forward_kinematics_keypoints', KinematicsPose, timeout = rospy.Duration(5.0))
        position = return_msg.pose.position
        position = torch.tensor([position.x, position.y, position.z])
        

    return position

def create_model(input_size, output_size):
    m_choices = [8, 16, 32, 64, 128]
    n_choices = [8, 16, 32, 64, 128, 256]
    l_choices = [8, 16, 32, 64, 128]
    layer_prob = [0.9, 0.6, 0.6]

    activation_choices = [nn.Sigmoid(), nn.Tanh(), nn.ReLU(), None]
    dropout_range = (0.0, 0.5)
  
    layers = [choice(m_choices), choice(n_choices), choice(l_choices)]
    act_f = choice(activation_choices)

    modules = []
    prev = input_size

    for layer_num, layer in enumerate(layers):

        if random() <= layer_prob[layer_num]:
            modules.append(nn.Linear(prev, layer))

            dropout = round(uniform(dropout_range[0], dropout_range[1]), 2)
            modules.append(nn.Dropout(dropout))

            if act_f is not None:
                modules.append(act_f)
            
            prev = layer 

    modules.append(nn.Linear(prev, output_size))

    if act_f is None and random() < 0.2:
        act_f = choice([x for x in activation_choices if x is not None]) 

    elif act_f is not None and random() <= 0.5:
        modules.append(act_f)
    
    sequential = nn.Sequential(*modules)
    
    return sequential

def optimal_pareto(history):
    values = []

    for idx, his in enumerate(history):
        value = his['min_loss'], his['pos_err']
        values.append(value)

    chain = pypareto.Comparison(pypareto.by_value, pypareto.MaxMinList(pypareto.MaxMin.MIN, pypareto.MaxMin.MIN)).as_chain()
    pareto_front = chain.split_by_pareto(values)[0]

    pareto_front_idx = [idx for idx, value in enumerate(values) if value in pareto_front]

    min_loss, min_pos = float('inf'), float('inf')
    best_model_loss, best_model_pos = None, None
    
    for idx, model in enumerate(history):
        if idx not in pareto_front_idx: continue
        
        if model['min_loss'] <= min_loss:
            min_loss = model['min_loss']
            best_model_loss = model

        if model['pos_err'] <= min_pos:
            min_pos = model['pos_err']
            best_model_pos = model
    
    print(f"Best Non-Dominated Positional Loss: {min_pos}")
    save_model(best_model_pos['model'], path = f"{MODEL_SAVE_PATH[:-3]}_positional_pareto.pt")

    print(f"Best Non-Dominated Joint Loss: {min_loss}")
    save_model(best_model_loss['model'], path = f"{MODEL_SAVE_PATH[:-3]}_joint_pareto.pt")
    
    if PLOT:
        for idx in pareto_front_idx:
            current = history[idx]
            print(f"Model: {current['model']}\nJoint: {current['min_loss']}, Positional: {current['pos_err']}\n")

        plt.scatter([value[0] for value in values], 
                    [value[1] for value in values], 
                    c = ["red" if value in pareto_front else "blue" for value in values])

        plt.xlabel("Joint Loss")
        plt.ylabel("Positional Loss")

        plt.show()

    return best_model_loss, best_model_pos

def run_nas(train_set, test_set):
    global LEARNING_RATE

    HISTORY = []

    lr_range = [1e-1, 5e-3, 1e-4, None]

    for iter in tqdm(range(NAS_iter)):
        print(f"Iteration {iter}")
        model = create_model(INPUTS, OUTPUTS)

        for initial_lr in lr_range:
            # optimizer = torch.optim.SGD(model.parameters(), lr=initial_lr, momentum=0.9)
            
            model = reinitialize_model(model)

            lr = initial_lr if initial_lr is not None else lr_range[0]
            optimizer = torch.optim.Adam(model.parameters(), lr = lr)            
            scheduler = None if initial_lr is not None else optim.lr_scheduler.StepLR(optimizer, step_size=20, gamma=0.1)

            best_loss = float('inf')
            for _ in range(EPOCHS):
                train(train_set, model, optimizer)
                test_loss = test(test_set, model)

                if scheduler is not None:
                    scheduler.step()

                if test_loss <= best_loss:
                    best_loss = test_loss

            pos_err = dataset_pos_error(test_set, model)

            model_iter = {
                "lr": initial_lr,
                "min_loss": best_loss,
                "pos_err": pos_err,
                "model": model,
                "model_state": model.state_dict()
            }
        
            HISTORY.append(model_iter)

            rospy.sleep(0.5)

    model1, model2 = optimal_pareto(HISTORY)
    
    evaluate_model(model1, train_set, test_set, name = "metrics_joint.json")
    evaluate_model(model2, train_set, test_set, name = "metrics_pos.json")


def reinitialize_model(model):
    for layer in model.children():
        if hasattr(layer, 'reset_parameters'):
            layer.reset_parameters()

    return model


def training_loop(train_set, test_set):
    global SCHEDULER
    
    # if RUN_NAS:
    #     run_nas(train_set, test_set)
    #     return    
    

    model = nn.Sequential(
        nn.Linear(4, 100),
        nn.ReLU(),
        nn.Linear(100, 4)
    )

    print(model)

    optimizer = torch.optim.Adam(model.parameters(), lr=1e-2)
    SCHEDULER = torch.optim.lr_scheduler.StepLR(optimizer, step_size=10, gamma=0.1)

    print("\nTraining Hyperparameters:")
    print(f"-> Model create with 7 inputs and 4 outputs")
    print(f"-> Training Epochs: {EPOCHS}")
    print(f"-> Adam Optimizer (initial learning rate of {LEARNING_RATE})")
    print(f"-> Cost Function based on {criterion} loss")
    print(f"-> Scheduler: {SCHEDULER}\n")

    ### Re-randomize the weights if model architecture loaded
    model = reinitialize_model(model)

    best_model = None
    history = {
        "train_loss": [], 
        "test_loss": [],
        "pos_test_loss": []
    }

    for epoch in range(EPOCHS):
        train_loss = train(train_set, model, optimizer)
        test_loss = test(test_set, model)

        if epoch % CHECKPOINT_LOG == 0:
            print(f"[Epoch {epoch} of {EPOCHS}]")
            print(f"LR: {optimizer.param_groups[0]['lr']}")
            print(f"Training Loss: {train_loss}, Test Loss: {test_loss}")
            

            pos_err = dataset_pos_error(test_set, model)

            print(f"Positional Test Loss: {pos_err}")
            print("-----------------------------------------\n")
            
            best_model = {
                'epoch': epoch,
                'train_loss': train_loss,
                'test_loss': test_loss,
                'pos_err': pos_err,
                'model': model
            }

            history['train_loss'].append(train_loss)
            history['test_loss'].append(test_loss)
            history['pos_test_loss'].append(pos_err)

        if SCHEDULER:
            SCHEDULER.step()

    best_model['history'] = history
    save_model(best_model['model'])

    return best_model

def load_model(path):
    model = torch.jit.load(path)
    return model

# takes in a module and applies the specified weight initialization
def weights_init_uniform(m):
    classname = m.__class__.__name__
    # for every Linear layer in a model..
    if classname.find('Linear') != -1:
        # apply a uniform distribution to the weights and a bias=0
        m.weight.data.uniform_(0.0, 1.0)
        m.bias.data.fill_(0)

if __name__ == '__main__':
    rospy.init_node("training")
    kinematics_pub = rospy.Publisher("evaluation", Joints, queue_size = 10)
    rospy.sleep(1)

    try:
        if validate_params():
            dataset = {}
            with open(DATASET_PATH, "rb") as input_file:
                dataset = pickle.load(input_file)
                print(f"There are {len(dataset)} points in the dataset")
            
            train_set, test_set = process_dataset(dataset)
            

            print("Training starting...")
            model_dict = training_loop(train_set, test_set)


            if EVALUATE:
                print("Evaluation starting...")
                evaluate_model(model_dict, train_set, test_set)

    except rospy.ROSException as e:
        rospy.logwarn(f"ROS Exception thrown: {e}")
