import numpy as np
import mediapipe as mp

from PIL import Image
import rospy
import cv2
import os
import pickle
import math
import vg
import torch
import tf

from mediapipe.framework.formats import landmark_pb2


CONTROL = rospy.get_param('/keypoint_collection_control/manipulator_control')

if not CONTROL:
    FILENAME = rospy.get_param('/keypoint_collection_control/filename')
    VIDEO_SAVE = rospy.get_param('/keypoint_collection_control/video_save') 
    save_file = os.path.expanduser(f"~/catkin_ws/src/open_manipulator_cv_controller/data/{FILENAME}")

else: 
    VIDEO_SAVE = False
    ik_model = rospy.get_param('/keypoint_collection_control/ik_model')
    CONTROL_MODEL = os.path.expanduser(f"~/catkin_ws/src/open_manipulator_cv_controller/data/models/{ik_model}")

MODEL = rospy.get_param('/keypoint_collection_control/gesture_model')
model_file = os.path.expanduser(f"~/catkin_ws/src/open_manipulator_cv_controller/data/{MODEL}")

ANGLE = None

# Dictionary of different modes/stages of data collection
COLLECTION_MODES = {
    "Setup": 0,
    "Calibration": 1,
    "Collection": 2,
    "Terminate": 3,
    "Finished": 4,
    "Control": 5,
}

MANIPULATOR_MAX_DIST = 0.38
CONFIDENCE_SCORE = 0.6

### Mediapipe Gesture Recognition
BaseOptions = mp.tasks.BaseOptions
GestureRecognizer = mp.tasks.vision.GestureRecognizer
GestureRecognizerOptions = mp.tasks.vision.GestureRecognizerOptions
VisionRunningMode = mp.tasks.vision.RunningMode

### Mediapipe pose estimation
mp_pose = mp.solutions.pose

### Mediapipe drawing
mp_drawing = mp.solutions.drawing_utils 
mp_drawing_styles = mp.solutions.drawing_styles
mp_ds = mp_drawing_styles.get_default_pose_landmarks_style()

### Arm Landmarks
wrist_keypoint = mp_pose.PoseLandmark.RIGHT_WRIST.value
elbow_keypoint = mp_pose.PoseLandmark.RIGHT_ELBOW.value
shoulder_keypoint = mp_pose.PoseLandmark.RIGHT_SHOULDER.value

arm_landmarks = [shoulder_keypoint, elbow_keypoint, wrist_keypoint]

def create_file():
    rospy.loginfo(f"Creating file: {save_file}")
    with open(save_file, 'wb') as handle:
        pickle.dump({}, handle, protocol=pickle.HIGHEST_PROTOCOL)

def save_dataset(data):
    rospy.loginfo("Saving the dataset...")
    print(data['points'])
    print(data['calib'])

    if not os.path.exists(save_file):
        create_file()

    # Read the file and append the input to it
    captured_keypoints = None
    with open(save_file, 'rb') as handle:
        captured_keypoints = pickle.load(handle)
        keys = list(captured_keypoints.keys())
        idx = keys[-1] + 1 if len(keys) != 0 else 0
        
        captured_keypoints[idx] = data
    
    if captured_keypoints:
        with open(save_file, 'wb') as handle:
            rospy.loginfo(f"Saving a video with index: {idx}")
            pickle.dump(captured_keypoints, handle, protocol=pickle.HIGHEST_PROTOCOL)
            rospy.loginfo(f"The dataset has been saved correctly at '{save_file}'")

def verify_gesture(gesture, expected_gesture, score):
    # Checking the the gesture is the expected gesture and if the confidence is high
    return gesture == expected_gesture and score >= CONFIDENCE_SCORE

def gesture_recognition(recognizer, capture, frame_timestamp_ms, mode):
    # Converting the image to Mediapipe image type
    pil_img = Image.fromarray(capture) 
    mp_image = mp.Image(image_format = mp.ImageFormat.SRGB, data = np.asarray(pil_img))
    
    # Using the model to get the gesture recognition from video
    gesture_recognition_result = recognizer.recognize_for_video(mp_image, frame_timestamp_ms)

    if len(gesture_recognition_result.gestures) != 0:
        # Getting the highest ranked gesture and its confidence score
        gesture = gesture_recognition_result.gestures[0][0].category_name
        score = gesture_recognition_result.gestures[0][0].score
        
        # If the mode is set to setup, searching for a Thumbs up to start calibration
        if mode == COLLECTION_MODES["Setup"]:
            if verify_gesture(gesture, "Thumb_Up", score):
                rospy.loginfo("Thumbs up detected...")
                rospy.loginfo("\n*** CALIBRATION MODE ***")
                return COLLECTION_MODES["Calibration"]
        # If the mode is set to collection, seraching for a Thumbs up or down to stop the collection
        elif mode == COLLECTION_MODES["Collection"]:
            if verify_gesture(gesture, "Thumb_Up", score):
                rospy.loginfo("Thumbs up detected...")
                rospy.loginfo("\n*** Collection Over ***")
                return COLLECTION_MODES["Finished"]
            if verify_gesture(gesture, "Thumb_Down", score):
                rospy.loginfo("Thumbs Down detected...")
                rospy.logwarn("\n***Program Terminated***")
                return COLLECTION_MODES["Terminate"]
        elif mode == COLLECTION_MODES["Control"]:
            if verify_gesture(gesture, "Thumb_Down", score):
                rospy.loginfo("Thumbs Down detected...")
                rospy.logwarn("\n***Program Terminated***")
                return COLLECTION_MODES["Terminate"]

    # Returing the mode
    return mode


def show_image(img, pose_landmarks = None):
    # Drawing the landmarks on the frame of the video (i.e. keypoints)
    if pose_landmarks:
        mp_drawing.draw_landmarks(
            img,
            pose_landmarks,
            landmark_drawing_spec=mp_ds
        )
    
    img = cv2.flip(img, 1)

    if ANGLE is not None:    
        img = cv2.putText(img, f'Wrist: {round(ANGLE, 2)}', (0, 30), cv2.FONT_HERSHEY_SIMPLEX, 
                        0.5, (0, 0, 255), 1, cv2.LINE_AA)
        
        cv2.putText

    # Displaying the current frame of the video
    cv2.imshow("Pose Video", img)

# TODO: Possibly add Visibility
def get_landmarks(pose_result):
    global ANGLE

    verification = not pose_result.pose_world_landmarks
    verification1 = not pose_result.pose_world_landmarks.landmark
    if verification or verification1: return None

    landmarks = {}
    angle = None
    for idx, keypoint in enumerate(pose_result.pose_world_landmarks.landmark):
        if idx in arm_landmarks:
            coordinates = (keypoint.x, keypoint.z, keypoint.y)
            landmarks[idx] = coordinates
    
    elbow, wrist = np.array(landmarks[elbow_keypoint]), np.array(landmarks[wrist_keypoint])
    wrist_vec = zero_out(wrist - elbow, 1)

    angle = vg.angle(wrist_vec, np.array([-1, 0, 0]), units="rad")
    
    if wrist_vec[2] < 0:
        angle = -angle
    
    angle = -angle
    
    landmarks['angle'] = angle
    ANGLE = angle
    
    return landmarks

def test_path(model_path):
    return os.path.exists(model_path)

############# HELPER FUNCTIONS FOR KEYPOINT MANIPULATION #############
 
### Check the visibility of a keypoint
def verify_visibility(keypoint, visibility_threshold):
    if keypoint.visibility <= visibility_threshold:
        return False

    return True

### Convert keypoint locations to a numpy array
def get_keypoint_as_np(keypoint):
    return np.array([keypoint.x, keypoint.y, keypoint.z])

### Takes in an array and zeros out a specified index
def zero_out(arr, n):
    new_arr = np.copy(arr)
    new_arr[n] = 0
    return new_arr

### Euclidean distance between two vectors
def euclid_dist(vec1, vec2):
  if vec1.size != vec2.size:
    rospy.logerr("Size of the two points doesn't match")
    return None
  
  sum_ = 0
  for i in range(vec1.size):
    squared_difference = (vec1[i] - vec2[i])**2
    sum_ += squared_difference
  
  return math.sqrt(sum_)

########################################################

def skeleton_estimation_pose(img, pose):
    # Running mediapipe to extract the keypoints
    results = pose.process(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))

    # Getting the pose landmarks from the resulting pose
    pose_landmarks = results.pose_landmarks
    
    if not pose_landmarks: 
        show_image(img)
        return None

    try:
        # Getting the right arm keypoints
        arm = [pose_landmarks.landmark[arm_keypoint] for arm_keypoint in arm_landmarks]
        pose_landmarks = landmark_pb2.NormalizedLandmarkList(landmark = arm)
    except Exception as e:
        rospy.logerr(f"Exception: {e}")

    show_image(img, pose_landmarks)

    return results


def calculate_calibration(pose_result):
    # Distance between the shoulder and the wrist
    landmarks = get_landmarks(pose_result)

    shoulder, wrist = landmarks[shoulder_keypoint], landmarks[wrist_keypoint]
    shoulder, wrist = np.array(shoulder), np.array(wrist)

    shifted_wrist = wrist - shoulder

    # Checking if the keypoint is to the left side of the shoulder, i.e. if x is positive (because the image is inverted, otherwise it should be positive)
    if shifted_wrist[0] > 0:
        rospy.logerr("Wrist has to be to the right of the shoulder")    
        return None

    distance = euclid_dist(np.array([0,0,0]), shifted_wrist)

    return MANIPULATOR_MAX_DIST / distance
  
def get_model(path):
    model = torch.jit.load(path)
    return model


GESTURE_CHECKPOINT = 15

def video_capture(control = False):
    video = cv2.VideoCapture(0)
    video_fps = video.get(cv2.CAP_PROP_FPS)

    counter, countdown = 0, -1
    threshold = (video_fps * 100) + 100
    calibration_results = []

    MODE = COLLECTION_MODES["Setup"]

    if VIDEO_SAVE is not None and type(VIDEO_SAVE) != bool:
        rospy.logfatal("video_save must be a boolean")
        return None

    if VIDEO_SAVE:
        data = {"points": [], "video":[], "calib": None}  
    else:
        data = {"points": [], "calib": None}

    if not control:
        rospy.logwarn("Important!")
        setup_info = """"\n\n*** Setup mode ***
To calibrate the arm length with the manipulator, face the camera, 
extend your **right** arm fully to the side (parallel to the ground) and gesture **Thumbs Up**.
Once the gesture is recognized, a countdown will initiate and at 0 it will calibrate the distance.
Important: Try moving the arm up and down whilst fully extended to capture most accurate max distance.\n\n"""
        rospy.loginfo(setup_info)
    else:
        model = get_model(CONTROL_MODEL)

    # Check if camera opened successfully
    if (not video.isOpened() or not video): 
        rospy.logerr("Error opening video stream or file")
        return
    
    options = GestureRecognizerOptions(
        base_options = BaseOptions(model_asset_path = model_file), 
        running_mode = VisionRunningMode.VIDEO
    )
    
    with mp_pose.Pose(static_image_mode=False, min_detection_confidence=0.5, model_complexity=2, enable_segmentation=True) as pose:    
        # Create a gesture recognizer instance with the video mode:

        with GestureRecognizer.create_from_options(options) as recognizer:
            frame_idx = 0

            # Running while the user does not signify that the data collection is finished
            while MODE != COLLECTION_MODES['Finished']:
                
                frame_idx += 1

                # If the program is shutdown or the user sets it to terminate, the data will not be saved 
                if MODE == COLLECTION_MODES['Terminate'] or cv2.waitKey(1) & 0xFF == ord('q') or rospy.is_shutdown(): 
                    # Release the video from memory and clear all the openCV windows
                    video.release()
                    cv2.destroyAllWindows()
                  
                    rospy.logwarn("Terminated the program, no data is saved")
                  
                    return None

                _, capture = video.read()
                
                try:   
                    frame_ms = int((1000 * frame_idx) / video_fps)
                    if frame_idx % GESTURE_CHECKPOINT == 0:
                        MODE = gesture_recognition(recognizer, capture, frame_ms, MODE)
                    
                    if MODE == COLLECTION_MODES["Setup"]:
                        show_image(capture)

                    elif MODE == COLLECTION_MODES['Calibration']:
                        counter += (1000 / video_fps) * 4
                        if counter <= threshold:
                            results = skeleton_estimation_pose(capture, pose)
                            if results is not None:
                                calibration = calculate_calibration(results)
                                if calibration is not None: calibration_results.append(calibration)

                            prev_count = countdown
                            countdown = int((counter)/1000)
                            
                            if prev_count != countdown:
                                print(3-countdown)
                    
                        else:
                            if len(calibration_results) == 0:
                                rospy.logerr("No results found for calibration, Terminating...") 
                                MODE = COLLECTION_MODES["Terminate"]
                                continue

                            data['calib'] = max(calibration_results)
                            MODE = COLLECTION_MODES["Collection"] if not control else COLLECTION_MODES["Control"]
                            
                            if MODE == COLLECTION_MODES["Collection"]:
                                rospy.loginfo("\n***Dataset Collection Started***\n\n-> Terminate: Thumbs Down (or CTRL+C)\n-> Finish and Save: Thumbs Up")
                            else:
                                rospy.loginfo("\n***Manipulation Started***\n\n-> Terminate: Thumbs Down (or CTRL+C)\n")

                    elif MODE == COLLECTION_MODES["Collection"]:
                        if VIDEO_SAVE:
                            data['video'].append(cv2.flip(capture, 1))
                        
                        pose_results = skeleton_estimation_pose(capture, pose)
                        
                        if not pose_results: continue

                        landmarks = get_landmarks(pose_results)
                        # Checking if landmarks exist and if all the three keypoints have been found
                        if landmarks:
                            data['points'].append(landmarks)

                    elif MODE == COLLECTION_MODES["Control"]:
                        pose_results = skeleton_estimation_pose(capture, pose)
                        if not pose_results: continue
                        landmarks = get_landmarks(pose_results)
                    
                        control_manipulator(landmarks, model, data['calib'])
                        

                except Exception as e:
                        # If there was an exception, the program should exit
                        rospy.logwarn(f"Exception thrown: {e}")
                        MODE = COLLECTION_MODES['Terminate']

    # Release the video from memory and clear all the openCV windows
    video.release()
    cv2.destroyAllWindows()

    return data

from open_manipulator_msgs.srv import SetJointPosition, SetJointPositionRequest

PATH_TIME = 0.5
SHOULDER = mp_pose.PoseLandmark.RIGHT_SHOULDER.value
WRIST = mp_pose.PoseLandmark.RIGHT_WRIST.value

def set_wrist_angle(joint_angles):
    rospy.wait_for_service('goal_joint_space_path', 2)
    set_joints = rospy.ServiceProxy('goal_joint_space_path', SetJointPosition)

    goal_request = SetJointPositionRequest()
    goal_request.joint_position.joint_name = ["joint1", "joint2", "joint3", "joint4"]
    goal_request.joint_position.position = joint_angles
    goal_request.joint_position.max_accelerations_scaling_factor = 0.0
    goal_request.joint_position.max_velocity_scaling_factor = 0.0
    goal_request.path_time = PATH_TIME
    resp = set_joints(goal_request)
    
    rospy.sleep(PATH_TIME)

    if not resp.is_planned:
        rospy.logerr("Failed to solve IK equation for wrist")

    return resp.is_planned


def shift_keypoints(original, offset):
    return (np.array(original) - np.array(offset))


def control_manipulator(landmarks, model, calib):
    ## Shifting the wrist to be relative to the shoulder at origin and applying calibration
    wrist_point = shift_keypoints(landmarks[WRIST], landmarks[SHOULDER]) * calib
    wrist_point[0] = -wrist_point[0]

    ## Concatenating the wrist point and angle to match the model input
    model_input = np.concatenate((wrist_point, -landmarks['angle']), axis = None)


    ## Predicting the joints based on the wrist point and angle
    control_outputs = model(torch.Tensor(model_input))

    control_outputs = control_outputs.tolist()

    ## Setting the manipulator's wrist angle
    _ = set_wrist_angle(control_outputs)

def verify_params():
    verified = test_path(model_file)
    if CONTROL:
        verified = verified and test_path(CONTROL_MODEL)

        if verified:
            ### Sleeping to let Gazebo launch
            rospy.sleep(3)
    return verified

if __name__ == '__main__':
    rospy.init_node("keypoint_collection_control")

    if verify_params():
        rospy.loginfo("All the parameters were verified")

        data = video_capture(control = CONTROL)
        
        if data: 
            save_dataset(data = data)
    else:
        rospy.logfatal(f"Check if all the provided model names are correct or if they are in the correct directories")
    
    rospy.loginfo("Finished")