import pickle
import rospy
import random
import tf
import cv2
import math
import os
import threading

import numpy as np
import mediapipe as mp
from scipy.spatial.transform import Rotation as R
from tqdm import tqdm

from tf.transformations import quaternion_matrix, translation_matrix
from geometry_msgs.msg import Point32
from geometry_msgs.msg import Pose
from open_manipulator_cv_controller.msg import JointPositions, ManipulatorPoses
from sensor_msgs.msg import PointCloud
from open_manipulator_msgs.msg import KinematicsPose
from open_manipulator_msgs.srv import SetKinematicsPose, SetJointPosition, SetJointPositionRequest

SIMULATE = rospy.get_param('/training_data/simulate')
VIDEO_SHOW = rospy.get_param('/training_data/show_video')

FILENAME = rospy.get_param('/training_data/input_keypoint_file')
TRAINING_FILE = rospy.get_param('/training_data/training_file')

OUT_OF_RANGE_THRESHOLD = 0.1
THRESHOLD = 2
NOISE = 0.025


input_file = os.path.expanduser(f"~/catkin_ws/src/open_manipulator_cv_controller/data/{FILENAME}")
training_file = os.path.expanduser(f"~/catkin_ws/src/open_manipulator_cv_controller/data/{TRAINING_FILE}")

mp_pose = mp.solutions.pose

# Defining ROS Node to publish the messages
frame = 'keypoint_frame'
node = 'keypoint_capture'

SHOULDER = mp_pose.PoseLandmark.RIGHT_SHOULDER.value
pub_shoulder = rospy.Publisher(f"{node}/shoulder", PointCloud, queue_size=50)

WRIST = mp_pose.PoseLandmark.RIGHT_WRIST.value
pub_wrist = rospy.Publisher(f"{node}/wrist", PointCloud, queue_size=50)

ELBOW = mp_pose.PoseLandmark.RIGHT_ELBOW.value
pub_elbow = rospy.Publisher(f"{node}/elbow", PointCloud, queue_size=50)

### Variables for moving the gripper
END_EFFECTOR_NAME = "gripper"
PATH_TIME = 1

### Starting joint positions and angles
START_LOCATION = (0.286, 0.0, 0.204)
START_ANGLE = 0.0

def shift_keypoints(original, offset):
    return (np.array(original) - np.array(offset))

def publish_point(keypoint, offset, timestamp, pub):
    point = Point32()
    shifted_keypoint = shift_keypoints(keypoint, offset)
    point.x, point.y, point.z = shifted_keypoint
    point.z = -point.z

    msg = PointCloud()
    msg.header.frame_id = frame
    msg.header.stamp = timestamp
    msg.points = [point]

    pub.publish(msg)

    return point

def set_manipulator(manipulator_pos):
    rospy.wait_for_service('goal_task_space_path_position_only', 2)
    
    set_kinematics_pose = rospy.ServiceProxy('goal_task_space_path_position_only', SetKinematicsPose)

    pose = Pose()
    pose.position.x = manipulator_pos[0]
    pose.position.y = manipulator_pos[1]
    pose.position.z = manipulator_pos[2]

    kinematics_pose = KinematicsPose()
    kinematics_pose.pose = pose

    resp = set_kinematics_pose(planning_group = 'arm', end_effector_name = END_EFFECTOR_NAME, kinematics_pose = kinematics_pose, path_time = PATH_TIME)
    
    if resp.is_planned:
        rospy.sleep(PATH_TIME)
    
    return resp.is_planned

def set_wrist_angle(angle, starting = False):
    if starting:
        rospy.wait_for_service('goal_joint_space_path', 2)
        set_joints = rospy.ServiceProxy('goal_joint_space_path', SetJointPosition)
    else: 
        rospy.wait_for_service('goal_joint_space_path_from_present', 2)
        set_joints = rospy.ServiceProxy('goal_joint_space_path_from_present', SetJointPosition)

    goal_request = SetJointPositionRequest()
    goal_request.joint_position.joint_name = ["joint1", "joint2", "joint3", "joint4"]
    goal_request.joint_position.position = [0.0, 0.0, 0.0, angle]
    goal_request.joint_position.max_accelerations_scaling_factor = 0.0
    goal_request.joint_position.max_velocity_scaling_factor = 0.0
    goal_request.path_time = PATH_TIME
    resp = set_joints(goal_request)

    rospy.sleep(PATH_TIME)

    if not resp.is_planned:
        rospy.logerr("Failed to solve IK equation for wrist")

    return resp.is_planned

def point_to_np(point):
    return np.array([point.x, point.y, point.z])

def np_to_point(arr):
    point = Point32()
    point.x, point.y, point.z = arr[0], arr[1], arr[2]
    return point

def homogenous_transformation(old_position, transform_matrix):
    # Converting the position to numpy array and appending one to it to make it a 4x1 vector
    old_position_vect = point_to_np(old_position)
    old_position_vect = np.append(old_position_vect, [1])
    old_position_vect = old_position_vect.reshape((4,1))

    # Multiplying the old point with the transform matrix to get the point in the other coordinate frame
    transformed_point = np.matmul(transform_matrix, old_position_vect)

    return np_to_point(transformed_point)

def create_matrix(rot, trans):
    # Converting the quaternion and position into matrices
    rot_matrix = quaternion_matrix(rot)
    trans_matrix = translation_matrix(trans)

    # Combining the two matrices to create a 4x4 homogenous transformation matrix
    rot_matrix = rot_matrix[:,:3]
    trans_matrix = np.array([trans_matrix[:,3]])
    transform_matrix = np.concatenate((rot_matrix, trans_matrix.T), axis=1)

    return transform_matrix

def broadcast(point, node_from, node_to, time, orientation = (0,0,0)):
    br = tf.TransformBroadcaster()

    br.sendTransform(
        (point.x, point.y, point.z),
        tf.transformations.quaternion_from_euler(orientation[0], orientation[1], orientation[2]),
        time,
        node_from,
        node_to
    )

def tf_listener(old_frame, new_frame, time):
    trans, rot = None, None
    listener = tf.TransformListener()

    try: 
        listener.waitForTransform(new_frame, old_frame, time, rospy.Duration(1.5))
        (trans, rot) = listener.lookupTransform(new_frame, old_frame,  time)
    except Exception as e:
        rospy.logwarn(f'Exception: {e}')

    return trans, rot

def create_transform_matrix(trans, rot):
    if not trans or not rot:
        return None
    
    return create_matrix(rot, trans)

def show_video(video):
    # Displaying the captured video frames 
    cv2.imshow("Test video", video)

def iterative_movement(current_loc, desired_loc):
    # Checks how many times the IK failed to be solved
    failure = 0
    backtracked = False
    # Iterating while we reach the set limit of failures
    while failure < (THRESHOLD+1): 
        # If the previous movement was successful, 
        # we attempt to move the arm to the desired location
        if failure == 0 or backtracked:
            # Trying to move to the desired location
            success = set_manipulator(desired_loc)
            
            # If desired location reached, finish
            if success: break

        # Calculating the midpoint between the desired location
        # and current location, and then trying to move there
        midpoint = (desired_loc + current_loc) / 2.
        success = set_manipulator(midpoint)

        # If failed to move to the midpoint, increase failure count
        if not success:
            failure += 1
            
            # Trying to move out, in case it is stuck
            if failure == THRESHOLD:
                
                if backtracked: break

                midpoint = (current_loc * 0.9)
                success = set_manipulator(midpoint)

                if not success:
                    failure += 1
                else:
                    backtracked = True
                    failure -= 1

        # Else change the current location to the midpoint and reset failures
        if success:
            current_loc = midpoint
            failure = 0
    
    # If moved to the desired location return True and the current location
    if failure == 0:
        rospy.logdebug(f"-> *** Reached Desired Location")
        return True, desired_loc
    
    # Else return False and the closest possible location
    rospy.logdebug(f"-> Failed to Reach the Destination - Final Location: {current_loc}\n")
    return False, current_loc

def euclid_dist(vec1, vec2):
  if vec1.size != vec2.size:
    print("Size of the two points doesn't match")
    return None
  
  sum_ = 0
  for i in range(vec1.size):
    squared_difference = (vec1[i] - vec2[i])**2
    sum_ += squared_difference
  
  return math.sqrt(sum_)

def range_convert(old_range, new_range, old_value):
    old_diff = (old_range[1] - old_range[0])
    new_diff = (new_range[1] - new_range[0])

    new_value = (((old_value - old_range[0]) * new_diff) / old_diff) + new_range[0]
    return new_value

### Getting the relative wrist
def get_relative_point(frame1, frame2, ros_time, point, rotation = True):

    trans, rot = tf_listener(frame1, frame2, ros_time)

    if rotation == False:
        rot = [0.0, 0.0, 0.0, 1.0]

    transform = create_transform_matrix(trans, rot)

    if transform is None:
        return None
    
    return homogenous_transformation(point, transform)

def save_point(dataset, input_point, output_point):
    dataset['x'].append(input_point)
    dataset['y'].append(output_point)
    return dataset

def verify_inputs():
    verify = True

    if type(SIMULATE) != bool or type(VIDEO_SHOW) != bool:
        rospy.logfatal("'simulate' and 'video_show' arguments must be booleans")
        verify = False

    if not os.path.exists(input_file):
        rospy.logfatal(f"The following file '{input_file}' does not exist")
        verify = False

    return verify

def create_kinematics_pose_msg(loc, angle):
    pose = Pose() 
    pose.position.x, pose.position.y, pose.position.z = loc[0], loc[1], loc[2]

    orientation = tf.transformations.quaternion_from_euler(0, angle, 0)
    pose.orientation.x, pose.orientation.y, pose.orientation.z, pose.orientation.w = orientation[0], orientation[1], orientation[2], orientation[3]

    kinematics_pose = KinematicsPose()
    kinematics_pose.pose = pose

    return kinematics_pose

def data_gather():
    # Reading the pickle file and saving its content as a dictionary
    ### TODO: Group Function
    rospy.loginfo("Reading the file...")
    
    with open(input_file, 'rb') as handle:
        captured_keypoints = pickle.load(handle)

    rospy.loginfo("File read successfully")

    kinematics_pub = rospy.Publisher('captured_keypoints', ManipulatorPoses, queue_size = 10)

    for idx in captured_keypoints.keys():
        rospy.loginfo(f"Processing video with {idx}")

        if 'points' not in captured_keypoints[idx] or 'calib' not in captured_keypoints[idx]:
            continue 
    
        points = captured_keypoints[idx]['points']
        rospy.loginfo(f"There are {len(points)} points")

        calib = captured_keypoints[idx]['calib']

        for point_idx, point in enumerate(points):
            if VIDEO_SHOW:
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    rospy.logwarn("Closing the video")
                    return
                
                video = captured_keypoints[idx]['video'][point_idx]
                show_video(video)


            if rospy.is_shutdown(): return
    
            angle = point['angle']

            broadcast_time_1 = rospy.Time.now()
            shoulder_point = publish_point(point[SHOULDER], point[SHOULDER], broadcast_time_1, pub_shoulder)
            wrist_point = publish_point(point[WRIST], point[SHOULDER], broadcast_time_1, pub_wrist)
            broadcast(shoulder_point, f"{node}/shoulder", frame, broadcast_time_1, orientation=(np.pi, 0.0, 0.0))
            relative_wrist = get_relative_point(frame, f"{node}/shoulder", broadcast_time_1, wrist_point)
            

            if relative_wrist is None:
                continue
        
            broadcast_time_2 = rospy.Time.now()
            broadcast(relative_wrist, f"{node}/wrist", f"{node}/shoulder", broadcast_time_2)
            manipulator_position = np_to_point(point_to_np(relative_wrist) * calib)
            relative_manipulator = get_relative_point(f"{node}/shoulder", f"{node}/wrist", broadcast_time_2, manipulator_position)
            elbow_point = publish_point(point[ELBOW], point[WRIST], broadcast_time_2, pub_elbow)

            if relative_manipulator is None:
                continue
            
            broadcast_time_3 = rospy.Time.now()
            broadcast(relative_manipulator, "/gripper/kinematics_pose", f"{node}/wrist", broadcast_time_3)
            broadcast(np_to_point(np.zeros((3,))), "/gripper/wrist", "/gripper/kinematics_pose", broadcast_time_3, orientation=(np.pi, angle, np.pi))

            # Getting the desired location
            desired_loc = point_to_np(relative_manipulator).flatten()
        

            original_loc = shift_keypoints(point[WRIST], point[SHOULDER]) * calib
            original_loc[0] = -original_loc[0]

            manipualtor_data = ManipulatorPoses()

            if random.random() < 0.75:
                for i in range(2):
                    random_angle = random.uniform(-math.pi, math.pi)
                    manipualtor_data.originalManipulatorPose = create_kinematics_pose_msg(original_loc, random_angle)
                    manipualtor_data.processedManipulatorPose = create_kinematics_pose_msg(desired_loc, random_angle)

                    lock.acquire()
                    kinematics_pub.publish(manipualtor_data)
                
                ## No angle
                if random.random() < 0.5:
                    manipualtor_data.originalManipulatorPose = create_kinematics_pose_msg(original_loc, 0.0)
                    manipualtor_data.processedManipulatorPose = create_kinematics_pose_msg(desired_loc, 0.0)

                    lock.acquire()
                    kinematics_pub.publish(manipualtor_data)

            manipualtor_data.originalManipulatorPose = create_kinematics_pose_msg(original_loc, angle)
            manipualtor_data.processedManipulatorPose = create_kinematics_pose_msg(desired_loc, angle)
            
            # Won't unlock until received a message back
            lock.acquire()
            kinematics_pub.publish(manipualtor_data)

def vector_angle(v1, v2):
    """ Angle between two 2D vectors """
    unit_v1 = v1 / np.linalg.norm(v1)
    unit_v2 = v2 / np.linalg.norm(v2)
    dot_product = np.dot(unit_v1, unit_v2)
    angle = np.arccos(dot_product)
    
    if v2[1] > 0:
        angle = -angle
    
    return angle

def process_joint_positions(joint_position):
    save = True if joint_position.success and len(joint_position.jointPositions) != 0 else False

    # print(f"Success: {joint_position.success}")

    if save:
        position = joint_position.manipulatorPose.pose.position

        if any([position.x, position.y, position.z]) != 0.0:
            
            joint_position.jointPositions[0].angle = vector_angle(np.array([1,0]), np.array([position.x, position.y])) 

            orientation = joint_position.manipulatorPose.pose.orientation
            joint_position.jointPositions[3].angle = tf.transformations.euler_from_quaternion([orientation.x, orientation.y, orientation.z, orientation.w])[1]

            TRAINING_DATA.append({
                "jointPositions": joint_position.jointPositions,
                "manipulatorPositions": position,
                "angle": orientation
            })
    # else:
        # rospy.logerr(f"Point out of range: {joint_position.manipulatorPose.pose.position}. DISTANCE: {dist}")  
    lock.release()


if __name__ == '__main__':
    lock = threading.Lock()
    TRAINING_DATA = []

    rospy.init_node(node)
    rospy.Subscriber("inverse_kinematics_keypoints", JointPositions, process_joint_positions)

    try: 
        if verify_inputs(): 
            data_gather()
    except rospy.ROSInterruptException as e:
        rospy.logwarn(f"Exception: {e}")
    
    lock.acquire()
    
    if len(TRAINING_DATA) != 0:
        rospy.loginfo(f"Saving training dataset with {len(TRAINING_DATA)} data samples into `{training_file}`")
        with open(training_file, 'wb') as handle:
            pickle.dump(TRAINING_DATA, handle, protocol=pickle.HIGHEST_PROTOCOL)
    
    else:
        rospy.logwarn(f"No training data")

    lock.release()

