
#include "open_manipulator_cv_controller/kinematics_solver.h"

KinematicsSolver::KinematicsSolver(bool using_platform, std::string usb_port, std::string baud_rate, double control_period)
{
    log::info("Setting up the IK Solver for Open Manipulator");

    open_manipulator_.initOpenManipulator(using_platform, usb_port, baud_rate, control_period);
    goal_joint_value_ = new std::vector<JointValue>();

    kinematics_ = new kinematics::SolverUsingCRAndSRPositionOnlyJacobian();
    open_manipulator_.addKinematics(kinematics_);
    log::info("Kinematics Solver Set 'SolverUsingCRandSRPoisionOnlyJacobian'");

    ik_pub_ = n_.advertise<open_manipulator_cv_controller::JointPositions>("inverse_kinematics_keypoints", 1000);
    fk_pub_ = n_.advertise<open_manipulator_msgs::KinematicsPose>("forward_kinematics_keypoints", 1000);

    log::info("Completed setting up the Kinematics Solver");
}

void KinematicsSolver::solveIK(Pose target_pose, const open_manipulator_msgs::KinematicsPose &manipulator_pose)
{
    open_manipulator_cv_controller::JointPositions msg;
    bool solved = open_manipulator_.solveInverseKinematics("gripper", target_pose, goal_joint_value_);

    int idx = 0;
    auto names = open_manipulator_.getManipulator()->getAllActiveJointComponentName();

    for (auto &point : *goal_joint_value_)
    {
        open_manipulator_cv_controller::JointAngle joint;
        joint.angle = point.position;
        joint.name = names.at(idx);
        msg.jointPositions.push_back(joint);
        
        // std::cout << joint.angle << std::endl;

        idx++;
    }
    msg.manipulatorPose = manipulator_pose;
    msg.success = solved;
    ik_pub_.publish(msg);
}

void KinematicsSolver::keypointsInverseCallback(const open_manipulator_cv_controller::ManipulatorPoses &msg)
{
    Eigen::Vector3d position;
    position[0] = msg.processedManipulatorPose.pose.position.x;
    position[1] = 0.0; // TODO: msg.pose.position.y;
    position[2] = msg.processedManipulatorPose.pose.position.z;

    Pose target_pose = {position};
    solveIK(target_pose, msg.originalManipulatorPose);
}

void KinematicsSolver::solveFK(const open_manipulator_cv_controller::Joints &msg)
{
    open_manipulator_msgs::KinematicsPose pub_msg;

    open_manipulator_.getManipulator()->setAllActiveJointPosition(msg.angles);
    open_manipulator_.solveForwardKinematics();

    auto gripper_position = open_manipulator_.getPose("gripper");
    pub_msg.pose.position.x = gripper_position.kinematic.position[0];
    pub_msg.pose.position.y = gripper_position.kinematic.position[1];
    pub_msg.pose.position.z = gripper_position.kinematic.position[2];

    fk_pub_.publish(pub_msg);
}

void KinematicsSolver::keypointsForwardCallback(const open_manipulator_cv_controller::Joints &msg)
{
    ros::Duration(0.1).sleep();
    solveFK(msg);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "kinematics_solver");

    KinematicsSolver kinematicsSolver(false, "/dev/ttyUSB0", "1000000", 0.010);
    auto n = kinematicsSolver.getNodeHandle();
    
    ros::Duration(1.0).sleep();
    ros::Subscriber sub_fk = n.subscribe("/evaluation", 100, &KinematicsSolver::keypointsForwardCallback, &kinematicsSolver);
    ros::Subscriber sub_ik = n.subscribe("/captured_keypoints", 100, &KinematicsSolver::keypointsInverseCallback, &kinematicsSolver);

    ros::spin();
    return 0;
}